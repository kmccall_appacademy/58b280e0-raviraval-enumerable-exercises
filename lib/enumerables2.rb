require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |long_string| long_string.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.delete!(" ")
  string.chars.select { |ch| string.count(ch) > 1 }.uniq.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  string.delete!(".,?!")
  string.split.sort_by { |word| word.length}[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  letters = ("a".."z")
  letters.reject { |ch| string.include?(ch) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  return true if year.to_s.chars == year.to_s.chars.uniq
  false
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select { |song| no_repeats?(song, songs) }.uniq
end

def no_repeats?(song_name, songs)
  songs.each_index do |idx|
    unless idx == songs.length - 1
      if songs[idx] == song_name && songs[idx + 1] == song_name
        return false
      end
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  return "" unless string.include?("c")
  string.delete!(",.,/?!:;'\"[]{}()-=_+'")
  string = string.split.select { |word| word.include?("c") }
  string.sort_by { |word| c_distance(word) }.first
end

def c_distance(word)
  word.reverse.length - word.index("c") - 1
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]
def repeated_number_ranges(arr)
  res = []
  idx = 0
  while idx < arr.length
    if arr[idx] == arr[idx + 1]
      res << [idx, find_end_repeat(arr, idx)[0]]
      idx += find_end_repeat(arr, idx)[1]
    else
      idx += 1
    end
  end
  res
  # arr.each_with_index do |el, idx|
  #   unless idx == arr.length - 1 || res.include?(idx)
  #     if el == arr[idx + 1]
  #       res << [idx, find_end_repeat(arr, idx)]
  #     end
  #   end
  # end
  # res
end

def find_end_repeat(arr, idx)
  jdx = idx
  el = arr[idx]
  until arr[jdx + 1] != el && jdx < arr.length
    jdx += 1
  end
  [jdx, jdx - idx]
end
